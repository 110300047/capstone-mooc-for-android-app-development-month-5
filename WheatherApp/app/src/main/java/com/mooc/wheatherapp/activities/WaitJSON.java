package com.mooc.wheatherapp.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;

import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;


import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.mooc.wheatherapp.R;
import com.mooc.wheatherapp.reciver.GetInfoReceiver;
import com.mooc.wheatherapp.service.GetInfoIntentService;

import java.util.ArrayList;



public class WaitJSON extends AppCompatActivity  {

    private static final String TAG="WHEATHERAPP-"+WaitJSON.class.getName();
    private static final int CODIGO_PERMISOS = 1;

    private GetInfoReceiver mInfoReciver=GetInfoReceiver.getInstance(this);
    private FusedLocationProviderClient fusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait_json);
        Log.d(TAG,"OnCreate");
        if (Build.VERSION.SDK_INT < 23){
            getWeather();
        }else{
            checkPermission();
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        Log.d(TAG,"OnStart");
        //mGoogleApiClient.connect();
    }

    @Override
    protected void  onPause(){
        super.onPause();
        Log.d(TAG,"OnPause");
        //mGoogleApiClient.disconnect();
    }


    private void checkPermission(){
        Log.d(TAG,"Check Permission");
        ArrayList<String> permissionNeeded=new ArrayList<>();
        /*if (!hasCoarsePermission()){
            permissionNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }*/
        if (!hasFinePermission()){
            permissionNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (permissionNeeded.size()>0){
            String[] permission=permissionNeeded.toArray(new String[permissionNeeded.size()]);
            ActivityCompat.requestPermissions(this,permission,CODIGO_PERMISOS);
        }else {
            getWeather();
        }
    }

    private void getWeather(){
        Log.d(TAG,"Echo");
        registerBroadcastReceiver();
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        final Context mContext=this;
        try {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                Log.d(TAG,"Lat: "+location.getLatitude());
                                Log.d(TAG,"Lon: "+location.getLongitude());
                                Intent intent=GetInfoReceiver.makeGetInfoIntent(mContext,location.getLatitude(),location.getLongitude());
                                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                            }
                        }
                    });
        }catch (SecurityException se){
            Log.d(TAG,"Error in securtiy"+se.getMessage());
        }

    }

    private boolean hasFinePermission(){
        return  (ContextCompat.checkSelfPermission
                (this,Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED );
    }
    private boolean hasCoarsePermission(){
        return  (ContextCompat.checkSelfPermission
                (this,Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED );
    }

    private void registerBroadcastReceiver() {
        IntentFilter intentFilter =
                new IntentFilter(GetInfoReceiver.GET_INFO);
        intentFilter.addAction(GetInfoReceiver.SET_INFO);

        LocalBroadcastManager.getInstance(this)
                .registerReceiver(mInfoReciver,
                        intentFilter);
    }

    /*public void sendJson(View view){
        //Toast.makeText(this,"Presionaste boton",Toast.LENGTH_LONG).show();
        final Context mContext=this;
        try {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                Log.d(TAG,"Lat: "+location.getLatitude());
                                Intent intent= new Intent(WaitJSON.GET_INFO);
                                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                            }
                        }
                    });
        }catch (SecurityException se){
            Log.d(TAG,"Error in securtiy"+se.getMessage());
        }
    }*/

    @Override
    protected void onDestroy(){
        // Always call super method.
        super.onDestroy();
        Log.d(TAG,"OnDestroy");
        LocalBroadcastManager.getInstance(this)
                .unregisterReceiver(mInfoReciver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG,"OnRequestPermissionsResult");
        switch (requestCode) {
            case CODIGO_PERMISOS:
                boolean hasPermission=true;
                for (int res :grantResults){

                    if(res!=PackageManager.PERMISSION_GRANTED){
                        hasPermission=false;
                    }
                }
                if (hasPermission){
                    getWeather();
                }else{
                    checkPermission();
                }
                break;
        }

    }


}
