package com.mooc.wheatherapp.reciver;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.mooc.wheatherapp.activities.MainActivity;
import com.mooc.wheatherapp.activities.WaitJSON;
import com.mooc.wheatherapp.service.GetInfoIntentService;

public class GetInfoReceiver extends BroadcastReceiver {

    private static GetInfoReceiver mInfoReciver;
    private static String TAG ="WHEATHERAPP-"+GetInfoReceiver.class.getName();
    private WaitJSON mContext;
    public static final String GET_INFO="com.mooc.wheatherapp.GET_INFO";
    public static final String SET_INFO="com.mooc.wheatherapp.SET_INFO";
    private static final String EXTRA_LAT="com.mooc.wheatherapp.LAT";
    private static final String EXTRA_LON="com.mooc.wheatherapp.LON";

    public static GetInfoReceiver getInstance(WaitJSON mContext){
        synchronized (GetInfoReceiver.class){
            if(mInfoReciver==null){
                mInfoReciver=new GetInfoReceiver(mContext);
                return mInfoReciver;
            }else {
                return mInfoReciver;
            }
        }
    }

    public  GetInfoReceiver(WaitJSON mContext) {
       this.mContext= mContext;
    }

    public static Intent makeGetInfoIntent(Context context,
                                        double lat,
                                        double lon) {
        return new Intent(GetInfoReceiver.GET_INFO)
                // Add extras.
                .putExtra(EXTRA_LAT, lat)
                .putExtra(EXTRA_LON, lon)
                // Limit receivers to components in this app's package.
                .setPackage(context.getPackageName());
    }



    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //Toast.makeText(context,"Presionaste boton",Toast.LENGTH_LONG).show();
        if (intent.getAction()!=null){
            switch (intent.getAction()){
                case GET_INFO:
                    Log.d(TAG,"En reciver get info");
                    Intent intentService= GetInfoIntentService.makeGetInfoServiceIntent(
                            context,
                            intent.getDoubleExtra(EXTRA_LAT,0),
                            intent.getDoubleExtra(EXTRA_LON,0)
                    );
                    context.startService(intentService);

                    break;
                case SET_INFO:
                    Log.d(TAG,"En reciver set info");
                    mContext.finish();
                    break;
                default:
                    break;
            }
        }
    }
}
