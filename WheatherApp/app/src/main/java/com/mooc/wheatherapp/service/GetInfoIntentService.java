package com.mooc.wheatherapp.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.util.Log;

import com.mooc.wheatherapp.connection.ApiConnectionUtils;
import com.mooc.wheatherapp.connection.ApiInterface;
import com.mooc.wheatherapp.models.WheatherInfo;

import java.io.IOException;

import retrofit2.Call;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 *
 * helper methods.
 */
public class GetInfoIntentService extends IntentService {

    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String EXTRA_LAT="com.mooc.wheatherapp.LAT";
    private static final String EXTRA_LON="com.mooc.wheatherapp.LON";
    private static final String TAG= "WHEATHERAPP-"+GetInfoIntentService.class.getName();

    private ApiInterface mApi;

    public static Intent makeGetInfoServiceIntent(Context context,
                                        double lat,
                                        double lon) {
        // Create and return an explicit intent that will start the
        // PongService.
        return new Intent(context, GetInfoIntentService.class)
                // Add extras.
                .putExtra(EXTRA_LAT, lat)
                .putExtra(EXTRA_LON, lon);
    }


    public GetInfoIntentService() {
        super("GetInfoIntentService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Log.d(TAG,"En service get info");
            mApi=ApiConnectionUtils.getCurrentWeather();
            WheatherInfo wheatherInfo=null;
            Call<WheatherInfo> call=mApi.getCurrentWeather(intent.getDoubleExtra(EXTRA_LAT,0),
                    intent.getDoubleExtra(EXTRA_LON,0));
            try {
                Log.d(TAG,"Haciendo peticion: "+call.request().url());
               wheatherInfo=call.execute().body();
            }catch (IOException e){
                Log.d(TAG,"Error en peticion a OPEN"+e.getMessage());
            }
            if (wheatherInfo!=null){
                Log.d(TAG,"INFO: "+wheatherInfo.getName());
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionFoo(String param1, String param2) {
        // TODO: Handle action Foo
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionBaz(String param1, String param2) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
