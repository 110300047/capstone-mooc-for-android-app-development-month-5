package com.mooc.wheatherapp.connection;

import com.mooc.wheatherapp.models.WheatherInfo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    String URL_BASE="https://api.openweathermap.org/data/2.5/";
    String OPEN_WEATHER_CURRENT="weather";
    String APPID="?APPID=42f02f86a781f4d3cd559c9794d0841b";

    @GET(OPEN_WEATHER_CURRENT+APPID)
    Call<WheatherInfo> getCurrentWeather (@Query("lat") double lat,@Query("lon") double lon);
}
