package com.mooc.wheatherapp.connection;


public class ApiConnectionUtils {
    private ApiConnectionUtils(){}

    public static ApiInterface getCurrentWeather(){
        return RetrofitClient.getClient().create(ApiInterface.class);
    }
}
