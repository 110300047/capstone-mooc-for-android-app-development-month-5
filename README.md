# Wheather App
> App that gets city weather.

## Why? [![start with why](https://img.shields.io/badge/start%20with-why%3F-brightgreen.svg?style=flat)](https://www.coursera.org/learn/aadcapstone/home/info)

This project is part of the Capstone MOOC for "Android App Development", that is, this project is the final project for this course.

The idea behind this application is to consult the weather, either from our city (current position), or from any city that is selected on the map.

## Explaining diagrams
![](map.png)

We start from the SplashActivity (left side), this activity will ask the user (depending on the Android version of the device) location permissions, to obtain the latitude and longitude of said device.
Later, a service will be created that will obtain the information in JSON format, once this information is obtained, the information will be sent to the BroadcastReciver so that the MainActivity can be invoked with this information.
Once in the MainActivity, you will have the option of choosing a city different from the current city, either by selecting it on a map, or by choosing a previously saved city (ContentProvider). If a map location is chosen, it will be saved in a ContentProvider, for later use (second option). Again, the latitude and longitude will be used to request the API for weather information and forward us through the broadcasreciver to the MainActivity.

![](diagram.png)

In the class diagram you can see the ApiService class that will get the information from the wheather API and send it to the broadcast reciver and this class will call the MainActivity.
You can also see the list of cities saved in the content provider.

## About API
The API accesses the current meteorological data for any location, passing the longitude and latitude as parameters in the url.

```url
api.openweathermap.org/data/2.5/weather?lat=35&lon=139
```

and it will return current weather information in JSON format:

```
{"coord": {"lon": 139, "lat": 35},
"sys": {"country": "JP", "sunrise": 1369769524, "sunset": 1369821049},
"weather": [{"id": 804, "main": "clouds", "description": "overcast clouds", "icon": "04n"}],
"principal": {"temperatura": 289.5, "humedad": 89, "presión": 1013, "temperatura_min": 287.04, "temperatura_max": 292.04},
"viento": {"velocidad": 7.31, "grados": 187.002},
"lluvia": {"3h": 0},
"nubes": {"todos": 92},
"dt": 1369824698,
"ID": 1851632,
"nombre": "shuzenji",
"cod": 200}
```

See more information about the API Wheather in the API doc in https://openweathermap.org/current.

